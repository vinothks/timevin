package com.vinothks.devops.timevin.com.vinothks.devops.timevin.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class HomeController {

    @RequestMapping(value = "/home")
    public String home(){
        return "Hello Vinoth";
    }

}
