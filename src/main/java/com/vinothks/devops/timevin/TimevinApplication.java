package com.vinothks.devops.timevin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimevinApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimevinApplication.class, args);
	}
}
